
# Create a user interface with react and redux that consumes auto part data from http://api.jsonbin.io/b/5ac79b198daf792844bf3523

### Notes:

* You can start with a boilerplate like create-react-app (recommended).
* Your app should include build scripts for local development and production
* Using redux with react: https://redux.js.org/basics/usage-with-react (consider using something like redux-thunk or redux-saga)

### Requirements:

* Fetch initial parts data from http://api.jsonbin.io/b/5ac79b198daf792844bf3523 (Hint: Redux/Axios)
* Render a list of the parts that displays the part name, and part price (Hint: don’t forget your map and keys)
* As a user, I want to select a part in the part list to see a singular view of the part that includes part image, part name, part price, and part description. (Hint: react-router)
* As a user, I want to “like” a part and see my “like” reflected in the user interface.

### Bonus

* As a user, I want to filter parts list by “liked” parts
* As a user, I want to remove parts from the parts list. (Make sure the list can be reset to initial state after parts have been removed)
* Use SCSS to add styles to UI (Don’t worry about design too much but feel free to add a style library like material-ui or ant)
* Check for linting errors using ESLint and Airbnb’s JS/React style guide
* Add persistent storage so the app maintains its state on refresh
* Feel free to add your own features

### Deliverable

* Use git for revision control
* Include readme with any relevant notes, explanations, and documentation
* Project can be published to GitHub or emailed as a zip file
