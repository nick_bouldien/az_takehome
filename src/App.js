import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';

import configureStore from './store/index';

import HomeContainer from './containers/HomeContainer';
import ItemDetailPageContainer from './containers/ItemDetailPageContainer';
import Header from './components/Header';

const store = configureStore();

class App extends Component {
  state = {}
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div className='container'>
            <Header />
            <Switch>

              <Route exact path='/' component={HomeContainer} />
              <Route exact path='/item/:sku' component={ItemDetailPageContainer} />
              {/* <Route exact path='/' component={Home} /> */}

              <Redirect to='/' />
            </Switch>
          </div>
        </BrowserRouter>
      </Provider>  
    );
  }
}

export default App;
