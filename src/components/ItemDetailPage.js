import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const ItemDetailPage = ({ item, match, likeFunction, removeFunction }) => {

  if (!item) { return <p>could not retrieve item details</p> }
  
  return (
    <React.Fragment>
      <img src={item.image} alt={`${item.name}`} />
      <ul>
        <li>{item.sku}</li>
        <li>Name: {item.name}</li>
        <li>likes: {item.likes}</li>
        <li>Price: ${item.price}</li>
        <li>{item.description}</li>

        {
          window.location.pathname.includes('item') ? ( // works for now...
            <React.Fragment>
              <button onClick={() => likeFunction(item.sku)}>like</button>
              <button onClick={() => removeFunction(item.sku)}>remove</button>
            </React.Fragment>
          ) : ( 
            <Link to={`/item/${item.sku}`}>Details</Link>
          )
        }  
      </ul>
    </React.Fragment>
  )
}

ItemDetailPage.propTypes = {
  item: PropTypes.shape({
    sku: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    likes: PropTypes.number,
  }),
  match: PropTypes.object,
  likeFunction: PropTypes.func,
  removeFunction: PropTypes.func,
}
export default ItemDetailPage;