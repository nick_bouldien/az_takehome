import React, { Component } from 'react';

import ItemList from '../components/ItemList';
import Loading from '../components/Loading';

class Home extends Component {
  state = {
    filter: false,
  }
  componentDidMount() {
    const itemsExist = this.props.items && this.props.items.length > 0;
    if (!itemsExist) {
      this.props.getItems();
    }
  }

  reset = () => {
    this.props.getItems();
  }

  // just simple local/ui state (not persisted)
  setFilter = () => {
    this.setState({
      filter: !this.state.filter
    });
  }

  render() {
    const { items, loading, error } = this.props;
    const { filter } = this.state;

    if (loading) { return <Loading /> }

    if (error) { return <code>{error.toString()}</code> }

    const filterItems = (items) => {
      if (!filter) { return items }
      return items.filter( item => item.likes > 0);
    }

    return (
      <div>
        <h2>Items:</h2>
        <button onClick={this.reset}>Reset</button>
        <br />
        {
          items ? (
            <div>
              <br />

              <button
                onClick={this.setFilter}
              >
                Show { filter ? 'only liked' : 'all' } items
              </button>

              <br />

              <ItemList items={filterItems(items)} />
            </div>
          ) : <p>No items in stock</p>
        }

      </div>
    );
  }
}

export default Home;
