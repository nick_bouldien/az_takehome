import React from 'react';
import { Link } from 'react-router-dom';

const Item = ({ item }) => (
  <React.Fragment>
    <img src={item.image} alt={`${item.name}`} />
    <ul>
      <li>Name: {item.name}</li>
      <li>Price: ${item.price}</li>
      {/* <button onClick={}>like</button> */}
      <Link to={`/item/${item.sku}`}>Details</Link>
    </ul>
  </React.Fragment>
);

export default Item;