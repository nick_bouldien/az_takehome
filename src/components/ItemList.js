import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ItemDetailPage from './ItemDetailPage';

class ItemList extends Component {
  state = {
    uiState: true,
    placeholder: 'yup',
  }

  render() {
    const { items } = this.props;

    return (
      items && items.map(item => <ItemDetailPage item={item} key={item.sku} /> )
    );
  }
}

ItemList.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      sku: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      price: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired,
      likes: PropTypes.number,
    })
  ),
}

export default ItemList;