import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => <h3><Link to="/">AZ.com</Link></h3>

export default Header;