import { combineReducers } from 'redux';
import omit from 'lodash/omit';
import {
  FETCHING_ITEMS,
  ITEMS_RECEIVED,
  RESET,
  TOGGLE_LIKE,
  HIDE_ITEM,
  // LIKE_ITEM,
  // ERROR_FETCHING,
} from '../actions/actionTypes';

const initialState =  {
   isFetching: false,
   items: {},
   error: null,
 }

const itemStock = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_ITEMS:
      return {
        ...state,
        isFetching: true
      };
    case ITEMS_RECEIVED:
      const items = action.payload;
      let itemByIdObject = {};
      
      for (let item of items) {
        itemByIdObject[item.sku] = {
          ...item,
          likes: 0,
        }
      }
      return {
        ...state,
        isFetching: false,
        items: itemByIdObject,
      };
    case TOGGLE_LIKE:
      const itemToUpdate = state.items[action.sku]; 

      const updatedItem = {
        ...itemToUpdate,
        likes: itemToUpdate.likes + 1,
      }

      return {
        ...state,
        items: {
          ...state.items,
          [action.sku]: updatedItem,
        }
      }
    case HIDE_ITEM:
      const newItems = omit(state.items, action.sku);
      window.location = ('/'); // should be moved ...
      return {
        ...state,
        items: {
          ...newItems,
        }
      }
    case RESET:
      return initialState;
    default:
      return state;
  }
};

const getItemIdsArray = (items) => {
  const itemIds = [];
  for (let item of items) {
    itemIds.push(item['sku'])
  }
  return itemIds;
};

const allIds = (state = [], action) => {
  switch (action.type) {
    case ITEMS_RECEIVED:
      return getItemIdsArray(action.payload);
    case HIDE_ITEM:
      // return handleToggle(state, action);
      console.log('stateeee: ', state);
      return state.filter(sku => sku !== action.sku)
    default:
      return state;
  }
};

const itemReducer = combineReducers({
  byId : itemStock,
  allIds,
});

export default itemReducer;