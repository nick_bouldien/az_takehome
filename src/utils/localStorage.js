// "borrowed" from dan abramov
// https://github.com/gaearon/todos/blob/03-persisting-state-to-local-storage/src/localStorage.js

export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state');
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    console.error('localstorage loadState err: ', err);
    return undefined;
  }
};

export const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('state', serializedState);
  } catch (err) {
    console.error('localstorage saveState err: ', err);
  }
};