import axios from 'axios';

import { API_URL } from './contants';

class Api {

	static async fetchItems() {
		const result = await axios.get(API_URL);

		return result.data;
	}

	// static async likeItem(itemId) {
    
	// 	return;
	// }
	
} 

export default Api;