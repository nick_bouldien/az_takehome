export const FETCHING_ITEMS = 'FETCHING_ITEMS';

export const ERROR_FETCHING = 'ERROR_FETCHING';

export const ITEMS_RECEIVED = 'ITEMS_RECEIVED';

export const RESET = 'RESET';

export const LIKE_ITEM = 'LIKE_ITEM';

export const TOGGLE_LIKE = 'TOGGLE_LIKE';

export const REMOVE_ITEM = 'REMOVE_ITEM';

export const HIDE_ITEM = 'HIDE_ITEM';