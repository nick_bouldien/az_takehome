import { delay } from 'redux-saga';
import { put, takeEvery, all, call, takeLatest /*, take, fork*/ } from 'redux-saga/effects';
import Api from '../api';

import {
  FETCHING_ITEMS,
  ITEMS_RECEIVED,
  LIKE_ITEM,
  TOGGLE_LIKE,
  REMOVE_ITEM,
  HIDE_ITEM,
  // ERROR_FETCHING,
  // RESET,
} from './actionTypes';


export default function* rootSaga() {
  yield all([
    watchFetchItems(),
    watchLikeItem(),
    watchRemoveItem(),
  ]);
}

/** watchers/helpers
 *  // need to takeEvery ???
*/
export function* watchFetchItems() {
  yield takeEvery(FETCHING_ITEMS, fetchItemsInStock);
}

export function* watchLikeItem() {
  yield takeEvery(LIKE_ITEM, likeItem);
}

export function* watchRemoveItem() {
  yield takeLatest(REMOVE_ITEM, removeItem);
}
/** sagas
*
*/
export function* fetchItemsInStock() {
  // while true nb???
  try {
    const items = yield call(Api.fetchItems);

    yield put({
      type: ITEMS_RECEIVED,
      payload: items
    });

  } catch (error) {
    // yield put({ type: ‘FETCH_FAILED’, error: error });
    console.error('Error fetching items: ', error);
  }
}

export function* likeItem(action) {
  const delayTime = Math.floor(Math.random()*700 + 300);
  yield delay(delayTime); // fake async with made up numbers
  try {    

    yield put({
      type: TOGGLE_LIKE,
      sku: action.sku,
    });

  } catch (error) {
    // yield put({ type: LIKE_ITEM_FAILED, error: error });
    console.error('Error liking item: ', error);
  }
}

export function* removeItem(action) {
  const delayTime = Math.floor(Math.random()*400 + 300);
  yield delay(delayTime); // fake async with made up numbers

  try {    

    yield put({
      type: HIDE_ITEM,
      sku: action.sku,
    });

  } catch (error) {
    // yield put({ type: REMOVE_ITEM_FAILED, error: error });
    console.error('Error removing item: ', error);
  }
}