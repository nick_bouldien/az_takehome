import { createStore, compose, applyMiddleware } from 'redux';
import { createLogger } from "redux-logger";
import createSagaMiddleware from 'redux-saga';

import throttle from 'lodash/throttle';
import rootReducer from '../reducers/index';
import rootSaga from '../actions/sagas';
import { loadState, saveState } from '../utils/localStorage';
// import { persistState } from "redux-devtools";

const sagaMiddleware = createSagaMiddleware();

const loggerMiddleware = createLogger();

const DEBUG = process.env !== 'production' && process.env.NODE_ENV !== 'production';

const middleware = [
  sagaMiddleware,
  DEBUG && loggerMiddleware,
].filter(Boolean);

const configureStore = () => {

  const persistedState = loadState();

  const store = createStore(
    rootReducer,
    persistedState,
    compose(
      applyMiddleware(
        ...middleware
      ),
      typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f
    )
  );

  store.subscribe(throttle(() => {
    saveState({
      itemReducer: store.getState().itemReducer,
    });
  }), 1000);

  sagaMiddleware.run(rootSaga);

  return store;
}

export default configureStore;
