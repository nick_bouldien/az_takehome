import { connect } from 'react-redux';
import Home from '../components/Home';

// probably need to move ...
// const filterLikedItems = (items, filterBy) => {
//   return items.filter(item => item[filterBy] > 0);
// }

const createArray = (itemObj) => {
  const itemArray = [];
  for (let item in itemObj) {
    itemArray.push(itemObj[item]);
  }

  return itemArray;
}

const mapStateToProps = (state /*, ownProps */) => {
  // const itemArray = createArray(state.itemReducer.byId.items);

  // if (state.filter) {
  //   filterLikedItems(state.itemReducer.byId.items, state.filter);
  // }

  return {
    loading: state.itemReducer.byId.isFetching,
    items: createArray(state.itemReducer.byId.items),
    error: state.itemReducer.byId.error,
    itemIds: state.itemReducer.allIds,
    // filter: state.filter,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getItems: () => {
    dispatch({ type: 'FETCHING_ITEMS' });
  },
  // showOnlyLikedItems: () => {
  //   dispatch({ type: 'SHOW_ONLY_LIKED' });
  // }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);