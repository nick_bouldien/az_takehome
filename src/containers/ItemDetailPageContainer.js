import { connect } from 'react-redux';
import ItemDetailPage from '../components/ItemDetailPage';


const mapStateToProps = (state, ownProps) => {
  const sku = ownProps.match.params.sku;
  const items = state.itemReducer.byId.items;
  return {
    item: items[sku],
  };
};

const mapDispatchToProps = (dispatch) => ({
  likeFunction: (sku) => {
    dispatch({
      type: 'LIKE_ITEM',
      sku,
    });
  },
  removeFunction: (sku) => {
    dispatch({
      type: 'REMOVE_ITEM',
      sku,
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ItemDetailPage);